#!/bin/bash

set -e

backup() {
  FILE="$1"
  DATE=$(date -r $FILE +%F)
  mv "$FILE" "$FILE.$DATE"
}

if [[ "$OSTYPE" == "darwin"* ]]; then
  CPU=$(sysctl -n hw.ncpu)
else
  CPU=$(nproc)
fi

(cd trunk && make -j$CPU "$@")

for i in *.img.gz; do
  [ -f "$i" ] && backup "$i"
done

mv -v ./trunk/bin/*/*/*/*-squashfs-* .
