#!/bin/bash
#
# -c           attempt to preserve all changed files in /etc/
# -n           do not save configuration over reflash

set -e

HOST=$(awk '/hostname/ {gsub(/'"'"'/, "", $3); print $3}' files/etc/config/system)
: ${BIN:="*-squashfs-sysupgrade.img.gz"}
: ${LAN:="root@$HOST.lan"}

tar -c $BIN | ssh -C "$LAN" "tar -xv -C /tmp && sh --login -i /sbin/sysupgrade $@ /tmp/$BIN"
