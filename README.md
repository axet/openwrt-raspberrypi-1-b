# OpenWrt

  * Super stable!
  * Super modular architecture!
  * Super supportive community!
  * Super awesome product!

Learn from them! Join!

  * https://openwrt.org

# How to install

  1. ./clone.sh
  2. ./up.sh
  3. ./make.sh

Flash your [Rapberry PI 1 B+](https://openwrt.org/toh/hwdata/raspberry_pi_foundation/raspberry_pi_bplus) and enjoy.

## How to Flash

1) After issuing last command ./make.sh you gonna get the following file:

`./openwrt-brcm2708-bcm2708-rpi-squashfs-factory.img.gz`

2) insert microSD into card reader and flush file with dd command.

`zcat openwrt-brcm2708-bcm2708-rpi-squashfs-factory.img.gz | sudo dd of=/dev/sdX bs=1M`

# Services enabled

  * UPnP (miniupnpd)
  * VPN
    * openvpn (192.168.54.40-50)
    * pptp (192.168.54.20-30)
  * IPTV (igmproxy)
  * WiFi (http://wiki.openwrt.org/doc/uci/wireless)
  * DHCP (dhcpd)
    * 192.168.54.100-250
  * SSH Server (dropbear)
  * Time Sync (ntpd)
  * Printers Server (p910nd)
    * Apple Bonjour (avahi-daemon)
  * BitTorrent tracker (opentracker)

# Configs

Following config shall be changed before issue a '../up && ./make.sh'. Those steps are nessesery mostly because those
configs holds credentials / passwords or specific settings filled by your ISP.

So please read carrefuly.

## network setup

Original config + my net and IPS setup. More info at:
  * http://wiki.openwrt.org/doc/uci/network

You need to use your 'eth0' mac address.

**_/etc/config/network_**

    config interface 'wan'
        option ifname 'eth0'
        option proto 'static'
        option ipaddr 'xxx.xxx.xxx.xxx'
        option netmask '255.255.255.0'
        option gateway 'xxx.xxx.xxx.xxx'
        option macaddr 'xx:xx:xx:xx:xx:xx'
        option dns '8.8.8.8 8.8.4.4 2001:4860:4860::8888 2001:4860:4860::8844'

## ipv6 teredo

**_/etc/config/miredo_**

    config miredo
        option enabled 1

**_/etc/config/firewall_**

    config defaults
        option masq6 '1'

**_/etc/config/network_**

    config globals globals
        option ula_prefix fc00:192:168:54::1/64

    config interface wan6
        option ifname 'teredo'
        option proto 'none'

## ipv6 tunnelbroker.net

**_/etc/config/network_**

    config interface wan6
        option proto 6in4
        option peeraddr '216.66.80.90'
        option ip6addr '2001:470:xx:xxx::2/64'
        option ip6prefix '2001:470:xx:xxx::/64'
        option tunnelid '222333'
        option username 'he_name'
        option password 'md5_password'

    config route 'ipv6'                                                           
            option interface 'wan'                                                
            option target '216.66.80.90'                                          
            option netmask '255.255.255.255'                                      
            option gateway '178.173.21.161'                                       
            option metric 0
        
## wifi setup

Pretty original config + my ssid name and password. Don't forget to change macaddr option, which is nessesery
to lookup interface. Or just skip it, after login run 'wifi' will configure your wifi automatically. You have to
use 'eth0 or wlan0' interface mac address for proper interface lookup.

  * https://openwrt.org/docs/guide-user/network/wifi/basic

**_/etc/config/wireless_**

    config wifi-device  radio0
        option macaddr  xx:xx:xx:xx:xx:xx
  
    config wifi-iface
        option device   radio0
        option network  lan
        option mode     ap
        option ssid     wifi.local
        option encryption 'psk2'
        option key      '12345678'

## VPN service

You can use booth openvpn and pptp vpns of you choose.

### openvpn

You have to prepare followings files in files/etc/openvpn/

    openssl dhparam -out dh1024.pem 1024

    openssl genrsa -out ca.key 4096
    openssl req -new -x509 -days 3650 -key ca.key -out ca.crt

    openssl genrsa -out server.key 4096
    openssl req -new -key server.key -out server.csr
    openssl x509 -req -days 3650 -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt

    openssl genrsa -out client.key 4096
    openssl req -new -key client.key -out client.csr
    openssl x509 -req -days 3650 -in client.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out client.crt

**_client.conf_**

    client
    dev tap

    proto udp
    remote openwrt.lan 1194
    resolv-retry infinite

    nobind

    persist-key
    persist-tun


    ca ca.crt
    cert client.crt
    key client.key
    comp-lzo

    script-security 2

### pptp

Set your pptp range, and your users name and pasword:

**_files/etc/config/pptpd_**

    config service 'pptpd'
            option 'enabled' '0'
            option 'localip' '192.168.54.1'
            option 'remoteip' '192.168.54.20-30'
                            
    config 'login'
            option 'username' 'user1'
            option 'password' 'password1'
        
## IPTV

Pretty original config. You have to check `logread` to see your ISP altnet mask. And allow firewall to pass
igmp trafic.

  * http://wiki.openwrt.org/doc/howto/udp_multicast

**_/etc/config/igmpproxy_**

    config phyint
        option network wan
        option direction upstream
        list altnet 77.94.170.0/24

## ntpd

Please fill up your timezone in the following config.

  * http://wiki.openwrt.org/doc/uci/system

**_/etc/config/system_**

    config system
        option hostname 'OpenWrt'
        option timezone 'MSK-4'

## ssh easy acess

Don't forget to make your live easier. Put your id-rsa.put into authorized_keys on the router.

**<i>/etc/dropbear/authorized_keys</i>**

    [your key]
    
## p910nd

Setting up printer is quite pain. First of all, you can't install cups stack since you gonna need more then 100MB of fonts and drivers to run it smoothly. Most routers has 4 - 8 MB flash images.

That is why, you have to passthrou your usb printer as network port printer.

  * http://wiki.openwrt.org/doc/howto/p910nd.server

To setup HP LaserJet 1020 you need one script and one firmware file. Details below:

  * [./files/etc/hotplug.d/usb/20-hp1020](./files/etc/hotplug.d/usb/20-hp1020)

## samba

Connect USB external harddrive formatted as ext4 or vfat :) to your router and fix block mount script. I recommend my external harddrive structure. It should have two folders _www_ and _local_. Booth have mutual access rw and ro. Readonly access opened by default, to change folder contentn you have to connect to the _www-rw_ hidden share or _local-rw_ share.

**_/etc/config/fstab_**

	config 'global'
	        option  anon_swap       '0'
	        option  anon_mount      '0'
	        option  auto_swap       '1'
	        option  auto_mount      '1'
	        option  delay_root      '5'
	        option  check_fs        '1'
	
	config 'mount'
	        option  target  '/mnt/shared'
	        option  uuid    '11111111-1111-1111-1111-111111111111'
	        option  enabled '1'
	        option 'options'  'rw,sync'
	        option 'enabled_fsck' '1'

**_/etc/config/samba_**

	config samba
	        option 'name'                   'OpenWrt'
	        option 'workgroup'              'WORKGROUP'
	        option 'description'            'OpenWrt'
	        option 'homes'                  '0'
	
	config 'sambashare'
	        option 'name' 'www'
	        option 'path' '/mnt/shared/www'
	        option 'guest_ok' 'yes'
	        option 'read_only' 'yes'
	
	config 'sambashare'
	        option 'name' 'www-rw'
	        option 'path' '/mnt/shared/www'
	        option 'guest_ok' 'yes'
	        option 'create_mask' '0700'
	        option 'dir_mask' '0700'
	        option 'browseable' 'no'
	
	config 'sambashare'
	        option 'name' 'local'
	        option 'path' '/mnt/shared/local'
	        option 'guest_ok' 'yes'
	        option 'read_only' 'yes'
	
	config 'sambashare'
	        option 'name' 'local-rw'
	        option 'path' '/mnt/shared/local'
	        option 'guest_ok' 'yes'
	        option 'create_mask' '0700'
	        option 'dir_mask' '0700'
	        option 'browseable' 'no'
	
