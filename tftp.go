package main

import "os"
import "log"
import "github.com/pin/tftp"

func main() {
	c, err := tftp.NewClient("192.168.54.242:69")
	if err != nil {
		panic(err)
	}
	log.Println("client connected")
	wt, err := c.Receive("wr842nv2_tp_recovery.bin", "octet")
	if err != nil {
		panic(err)
	}
	log.Println("file received")
	file, err := os.Create("test")
	if err != nil {
		panic(err)
	}
	// Optionally obtain transfer size before actual data.
	if n, ok := wt.(tftp.IncomingTransfer).Size(); ok {
		log.Printf("Transfer size: %d\n", n)
	}
	n, err := wt.WriteTo(file)
	if err != nil {
		panic(err)
	}
	log.Printf("%d bytes received\n", n)
}

