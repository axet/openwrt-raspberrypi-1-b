#!/bin/bash

set -e

# ubuntu packages
which apt-get && sudo apt-get install -y libncurses5-dev gawk subversion zlib1g-dev libssl-dev libxml-parser-perl

git clone -b v19.07.8 https://git.openwrt.org/openwrt/openwrt.git trunk
